
namespace ServerCS.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    
    // This isn't useful, especially because I'm using Infer.NET. I just
    // wanted to write it down somewhere.
    
    public static class Probabilities
    {
        /// <summary>
        /// Bayes' formula, `P(A|B) = (P(B|A) * P(A)) / P(B)`. Returns `P(A|B)`.
        /// </summary>
        /// <param name="b_if_a">P(B|A)</param>
        /// /// <param name="a">P(A)</param>
        /// <param name="b">P(B)</param>
        /// <returns>P(A|B) (`(b_if_a * a) / b`)</returns>
        public static double Bayes(double b_if_a, double a, double b)
        {
            return (b_if_a * a) / b;
        }
    }
}
