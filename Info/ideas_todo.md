
First experiment: Sentence predictor. Predict what member of chat will post next and what they'll say.
Parameters: member, message, channel, time, date, day of week, somehow the context of images? sentiment/emotional context?
Notes: 
 - Make a discord command to predict what the next thing said will be, ignoring the command message itself.
 - Try to normalize/sanitize messages a little. Maybe run multiple experiments with varying levels of
sanitization. One where the message is in-tact, one where capitalization is removed, one where all
non-alphanumeric characters are removed.
 - This isn't going to be a single model, it's going to be several layers of models, from trying to figure out
 if the input is even valid, to it's context, conversational and emotional.

Misc: 
 - Add an AI-powered random rage comic generator.
